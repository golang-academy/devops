# Terraform

## Terraform Commands
```
# Install Terraform
snap install terraform --classic

# Verify version
terraform version

# Initialize given provider
terraform Initialize

# Validate HCL code
terraform validate

# Check formatting
terraform fmt

# Plan what to do
terraform plan

# Apply resources based on tf code
terraform apply

# Destory resources
terraform destroy --auto-approve

# Check what's deployed
terraform state list
```
