# This is my first EC2 Instance
resource "aws_instance" "web" {
  count         = 10
  ami           = "ami-0b93ce03dcbcb10f6"
  instance_type = "t2.micro"
  user_data     = file("postinstall.sh")

  tags = {
    Owner = "pbarczi"
  }
}
